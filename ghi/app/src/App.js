import React from 'react';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Nav from './Nav';
import MainPage from './MainPage';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConference from './AttendConference';
import PresentationForm from './PresentationForm';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="locations/new" element={<LocationForm />} />
          <Route path="conferences/new" element={<ConferenceForm />} />
          <Route path="presentations/new" element={<PresentationForm />} />
          <Route path="attendees/new" element={<AttendConference />} />
          <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
