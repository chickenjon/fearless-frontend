function createCard(name, location, description, pictureUrl, starts, ends) {
    return `
        <div class="col">
        <div class="card mt-4 shadow">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
            <small>${starts} - ${ends}</small>
            </div>
        </div>
        </div>
    `;
}

function formatdate(isostring) {
    const date = isostring.substring(0, 10);
    const arr = date.split("-");
    const month = parseInt(arr[1], 10).toString();
    const day = parseInt(arr[2], 10).toString();
    const year = arr[0];
    return `${month}/${day}/${year}`;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error('Response not ok');
        } else {
            const data = await response.json();

            const column = document.querySelector('.row');
            for (let i in data.conferences) {
                column.innerHTML += `
                    <div class="col">
                        <div class="card mt-4 shadow">
                            <img height="300px" class="card-img-top">
                                <div class="card-body">
                                    <h5 class="card-title placeholder-glow">
                                        <span class="placeholder col-6"></span>
                                    </h5>
                                    <h6 class="card-subtitle mb-2 text-muted placeholder-glow">
                                        <span class="placeholder col-4"></span>
                                    </h6>
                                    <p class="card-text placeholder-glow">
                                        <span class="placeholder col-7"></span>
                                        <span class="placeholder col-4"></span>
                                        <span class="placeholder col-4"></span>
                                        <span class="placeholder col-6"></span>
                                        <span class="placeholder col-8"></span>
                                    </p>
                                </div>
                                <div class="card-footer">
                                    <small class="placeholder-glow"><span class="placeholder col-4"></span></small>
                                </div>
                        </div>
                    </div>
                `
            }
            
            let placeholders = document.querySelectorAll(".col");
            let i = 0;
            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const location = details.conference.location.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = formatdate(details.conference.starts);
                    const ends = formatdate(details.conference.ends);
                    const html = createCard(name, location, description, pictureUrl, starts, ends);
                    placeholders[i].innerHTML = html;
                    i++;
                }
            }

        }
    } catch (e) {
        const alertBox = document.getElementById('alert');
        let wrapper = document.createElement('div')
        wrapper.innerHTML = '<div class="alert alert-warning alert-dismissible" role="alert">You messed something up man.<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'
        alertBox.append(wrapper)
    }

});